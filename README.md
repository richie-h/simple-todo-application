# README #

### What is this repository for? ###

This repository was initially developed to showcase programming styles to potential employers. Provided for reference and as a simple application to be integrated in your future applications.

Version 0.1

### How do I get set up? ###

You'll need MySQL in order to get the application to work. Table schema has been provided in the sql folder. Once this table schema has been setup, you will need to change the Database name, username and password in the Database PDO (include/Database.class.php)

### TODO ###

* Create Tests to verify the output
* Implement a Login function